<?xml version="1.0"?>
<!-- This URDF was automatically created by SolidWorks to URDF Exporter! Originally created by Stephen Brawner (brawner@gmail.com) 
     Commit Version: 1.6.0-1-g15f4949  Build Version: 1.6.7594.29634
     For more information, please see http://wiki.ros.org/sw_urdf_exporter -->
<robot xmlns:xacro="http://www.ros.org/wiki/xacro" name="agv_robot">
  <!-- Include gazebo configuration -->
  <xacro:include filename="$(find agv_description)/urdf/agv.gazebo.xml"/>

  <!-- Robot Parameter -->
  <xacro:property name="base_width" value="0.5"/>
  <xacro:property name="base_len" value="1.15"/>
  <xacro:property name="base_height" value="0.3"/>
  <xacro:property name="base_distance_from_ground" value="0.0125"/>
  <xacro:property name="base_mass" value="5"/>

  <xacro:property name="wheel_radius" value="0.065"/>
  <xacro:property name="wheel_width" value="0.035"/>
  <xacro:property name="wheel_mass" value="10"/>
  <xacro:property name="wheel_separation" value="0.462"/>

  <xacro:property name="caster_wheel_radius" value="0.0375"/>
  <xacro:property name="caster_wheel_width" value="0.11"/>
  <xacro:property name="caster_wheel_mass" value="1"/>
  <xacro:property name="caster_wheel_joint_offset" value="0.0738"/> <!-- default 0.0735 -->

  <xacro:property name="lift_mass"  value = "1" />
  <xacro:property name="lift_width"  value = "1.1" />
  <xacro:property name="lift_height"  value = "0.05" />
  <xacro:property name="lift_depth"  value = "0.08" />

    <!--Interial macros-->
    <xacro:macro name="cylinder_inertia" params="m r h">
        <inertial>
            <origin xyz="0 0 0" rpy="0 0 0" />
            <mass value="${m}"/>
            <inertia ixx="${m*(3*r*r+h*h)/12}" ixy="0" ixz="0"
            iyy="${m*(3*r*r+h*h)/12}" iyz="0" izz="${m*r*r/2}"/>
        </inertial>
    </xacro:macro>

    <xacro:macro name="box_inertia" params="m w h d">
        <inertial>
            <origin xyz="0 0 0" rpy="0 0 0" />
            <mass value="${m}"/>
            <inertia ixx="${m / 12.0 * (d*d + h*h)}" ixy="0.0" ixz="0.0"
            iyy="${m / 12.0 * (w*w + d*d)}" iyz="0.0"
                     izz="${m / 12.0 * (w*w + h*h)}"/>
        </inertial>
    </xacro:macro>

  <!-- Base footprint is on the ground under the robot -->
  <link name="base_footprint"/>

  <!-- dummy_link -->
  <link name="dummy_link">
    <xacro:box_inertia m="${base_mass*2}" w="${base_len}" h="${base_width}" d="${base_height/81}"/>
  </link>

  <!-- base_link - base_footprint Joint-->
  <joint name="dummy_joint" type="fixed">
    <origin
      xyz="0 0 0"
      rpy="0 0 0"/>
    <parent link="base_footprint"/>
    <child link="dummy_link"/>
  </joint>

  <!-- base_link -->
  <link
    name="base_link">
    <xacro:box_inertia m="${base_mass}" w="${base_len}" h="${base_width}" d="${base_height/81}"/>

    <visual>
      <origin
        xyz="0 0 0"
        rpy="0 0 0" />
      <geometry>
        <mesh
          filename="package://agv_description/meshes/base.STL"/>
      </geometry>
      <material
        name="">
        <color
          rgba="0.8 0.5 0 0.5" />
      </material>
    </visual>
    <collision>
      <origin
        xyz="0 0 0"
        rpy="0 0 0" />
      <geometry>
        <mesh
          filename="package://agv_description/meshes/base.STL"/>
      </geometry>
    </collision>
  </link>

    <!-- base_link - base_footprint Joint-->
    <joint name="base_footprint_joint" type="fixed">
        <origin
          xyz="0 0 ${base_distance_from_ground + base_height/2}"
          rpy="0 0 0"/>
        <parent link="base_footprint"/>
        <child link="base_link"/>
    </joint>

    <!-- Wheel and joint macro -->
    <xacro:include filename="$(find agv_description)/urdf/wheel.urdf.xacro"/>

    <!-- Create left and right wheel/joint -->
    <xacro:wheel name="right"
                 inertial_xyz="0 0 0"
                 visual_rpy="0 0 0"
                 joint_xyz="0 ${-wheel_separation/2} ${wheel_radius - base_distance_from_ground - (base_height/2)}" joint_rpy="${-radians(90)} 0 0"/>
    <xacro:wheel name="left"
                 inertial_xyz="0 0 0"
                 visual_rpy="0 ${radians(180)} 0"
                 joint_xyz="0 ${wheel_separation/2 } ${wheel_radius - base_distance_from_ground - (base_height/2)}"  joint_rpy="${-radians(90)} 0 0" />
    <!-- Caster wheel and joint macro -->
    <xacro:include filename="$(find agv_description)/urdf/caster.urdf.xacro"/>

    <!-- Create 3 caster wheels -->
    <xacro:caster name="front_left"     inertial_xyz="0 0 0"    joint_xyz="${base_len/2 - 0.09} 0.15 ${wheel_radius - base_distance_from_ground - (base_height/2) + caster_wheel_joint_offset + caster_wheel_radius - wheel_radius}"/>
    <xacro:caster name="front_right"    inertial_xyz="0 0 0"    joint_xyz="${base_len/2 - 0.09} -0.15 ${wheel_radius - base_distance_from_ground - (base_height/2) + caster_wheel_joint_offset + caster_wheel_radius - wheel_radius}"/>
    <xacro:caster name="back_left"      inertial_xyz="0 0 0"    joint_xyz="${-(base_len/2 - 0.09)} 0.15 ${wheel_radius - base_distance_from_ground - (base_height/2) + caster_wheel_joint_offset + caster_wheel_radius - wheel_radius}"/>
    <xacro:caster name="back_right"     inertial_xyz="0 0 0"    joint_xyz="${-(base_len/2 - 0.09)} -0.15 ${wheel_radius - base_distance_from_ground - (base_height/2) + caster_wheel_joint_offset + caster_wheel_radius - wheel_radius}"/>

    <!-- Lidar link and joint macro -->
    <xacro:include filename="$(find agv_description)/urdf/lidar.urdf.xacro"/>
    <!-- Create lidar -->
    <xacro:lidar name="lidar" inertial_xyz="0 0 0" joint_xyz="0.5473 0 -0.02"/>


    <!-- Lifted mechanism -->
    <xacro:include filename="$(find agv_description)/urdf/lift.urdf.xacro"/>
    <xacro:lift name="left"     inertial_xyz="0 0 0"    joint_xyz="1 1 1"/>
    <xacro:lift name="right"     inertial_xyz="0 0 0"    joint_xyz="2 2 1"/>
</robot>
